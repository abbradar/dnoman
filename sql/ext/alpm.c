#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <string.h>
#include <alpm.h>
#include <alloca.h>

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

char* from_text(text const* a, char* r) {
  memcpy(r, VARDATA(a), VARSIZE(a));
  r[VARSIZE(a)] = '\0';
  return r;
} 

PG_FUNCTION_INFO_V1(vercmp);
Datum vercmp(PG_FUNCTION_ARGS) {
  text *arg1 = PG_GETARG_TEXT_P(0);
  char *r1 = alloca(VARSIZE(arg1) + 1);
  memcpy(r1, VARDATA(arg1), VARSIZE(arg1));
  r1[VARSIZE(arg1)] = '\0';

  text *arg2 = PG_GETARG_TEXT_P(1);
  char *r2 = alloca(VARSIZE(arg2) + 1);
  memcpy(r2, VARDATA(arg2), VARSIZE(arg2));
  r2[VARSIZE(arg2)] = '\0';

  int16 r = alpm_pkg_vercmp(r1, r2);
  
  PG_RETURN_INT16(r);
}
