begin;

drop function if exists vercmp(text, text);

create function vercmp(text, text) returns smallint
    as 'alpm', 'vercmp'
    language C immutable strict
    cost 30;

commit;
