begin;

drop schema public cascade;
create schema public;

create table repositories (
        id serial primary key,
        name varchar not null unique,
        description text not null default '',
        priority int not null default 0,
        active bool not null default true
);

create table mirrors (
        id serial primary key,
        url varchar not null,
        system bool not null default false
);

create table repo_mirrors (
        mirror int not null references mirrors(id) on delete cascade,
        repo int not null references repositories(id) on delete cascade,
        priority int not null default 0,
        primary key (mirror, repo)
);

create table packages (
        id serial primary key,
        name varchar not null unique,
        description varchar not null,
        url varchar not null,
        replaced_by int references packages(id) on delete restrict
);

create table groups (
        id serial primary key,
        name varchar not null unique,
        description text not null default ''
);

create table package_groups (
        package int not null references packages(id) on delete cascade,
        "group" int not null references groups(id) on delete cascade,
        primary key ("group", package)
);

create table architectures (
        id serial primary key,
        name varchar(32) not null unique
);

create table package_versions (
        id serial primary key,
        package int not null references packages(id) on delete cascade,
        repo int not null references repositories(id) on delete cascade,
        arch int not null references architectures(id) on delete cascade,
        filename varchar not null,
        version varchar(32) not null,
        license varchar not null,
        packager varchar not null,
        sha256sum char(64) not null,
        csize bigint not null check(csize >= 0),
        size bigint not null check(size >= 0),
        ts timestamp not null,
        unique (package, repo, version, arch)
);

create table package_archs (
        package int not null references packages(id) on delete cascade,
        arch int not null references architectures(id) on delete cascade,
        last_version int not null references package_versions(id),
        primary key (package, arch)
);

create function update_last_version ()
    returns trigger
    language plpgsql
    as $$
    declare current_last varchar;
    begin
      current_last := (select version from package_archs left join package_versions
          on package_versions.id = package_archs.last_version
      where package_archs.package = NEW.package and package_archs.arch = NEW.arch);
      if current_last is null then
        insert into package_archs (package, arch, last_version) values (NEW.package, NEW.arch, NEW.id);
      else
        if vercmp(NEW.version, current_last) = 1 then
          update package_archs set last_version = NEW.id where package = NEW.package and arch = NEW.arch;
        end if;
      end if;
      return null;
    end;
    $$;

create function remove_last_version ()
    returns trigger
    language plpgsql
    as $$
    declare current_last int;
    begin
      current_last := (select latest_of_packages(array(select id from package_versions
              where package = OLD.package and arch = OLD.arch)));
      if current_last is null then
        delete from package_archs where package = OLD.package and arch = OLD.arch;
      else
        update package_archs set last_version = current_last
        where package = OLD.package and arch = OLD.arch;
      end if;
      return null;
    end;
    $$;

create trigger "01_remove_package_version"
    after delete or update of package
    on package_versions
    for each row
    execute procedure remove_last_version ();

create trigger "02_insert_package_version"
    after insert or update of package, version
    on package_versions
    for each row
    execute procedure update_last_version ();

create type constraint_type as enum ('>=', '>', '<=', '<', '=', '<>');
create type dependency_type as enum ('offers', 'depends', 'conflicts', 'provides');

create table dependencies (
        id serial primary key,
        package int not null references package_versions(id) on delete cascade,
        dep int not null references packages(id) on delete restrict,
        type dependency_type not null,
        description text not null default '',
        version_constraint constraint_type,
        version varchar(32),
        unique (package, dep, type),
        check (version_constraint is null and version is null
            or version_constraint is not null and version is not null)
);

create table files (
        id serial primary key,
        package int not null references package_versions(id) on delete cascade,
        file_name text not null,
        unique (package, file_name)
);

-- procedures

create function latest_of_packages (ids int[])
    returns int
    language plpgsql
    stable
    strict
    as $$
    declare previd int := 1;
    declare curr varchar;
    declare prev varchar;
    begin
      if coalesce(array_length(ids, 1), 0) = 0 then
        return null;
      else
        prev := (select version from package_versions where id = ids[previd]);
        for i in 2 .. array_length(ids, 1) loop
          curr := (select version from package_versions where id = ids[i]);
          if vercmp(prev, curr) = -1 then
            prev := curr;
            previd := i;
          end if;
        end loop;
        return ids[previd];
      end if;
    end;
    $$;

create function match_dependencies (did int, parch int)
    returns table (id int)
    language plpgsql
    stable
    strict
    as $$
    declare
    pid int := (select dep from dependencies where dependencies.id = did);
    replaced int := (select replaced_by from package_versions left join packages
                            on packages.id = package_versions.package
                     where package_versions.id = pid);
    was_replaced bool := replaced is not null;
    cnst constraint_type := (select version_constraint from dependencies where dependencies.id = did);
    ver varchar;
    begin
      while replaced is not null loop
        pid := replaced;
        replaced := (select replaced_by from package_versions left join packages
                           on packages.id = package_versions.package
                     where package_versions.id = pid);
      end loop;
      if cnst is null or was_replaced then
        return query select package_versions.id from package_versions where package = pid and arch = parch;
      else
        ver := (select version from dependencies where dependencies.id = did);
        case cnst
          when '>=' then
            return query select package_versions.id from package_versions where package = pid
            and arch = parch and vercmp(version, ver) >= 0;
          when '>' then
            return query select package_versions.id from package_versions where package = pid
            and arch = parch and vercmp(version, ver) > 0;
          when '<=' then
            return query select package_versions.id from package_versions where package = pid
            and arch = parch and vercmp(version, ver) <= 0;
          when '<' then
            return query select package_versions.id from package_versions where package = pid
            and arch = parch and vercmp(version, ver) < 0;
          when '=' then
            return query select package_versions.id from package_versions where package = pid
            and arch = parch and vercmp(version, ver) = 0;
          when '<>' then
            return query select package_versions.id from package_versions where package = pid
            and arch = parch and vercmp(version, ver) <> 0;
        end case;
      end if;
    end;
    $$;

create function find_dependencies_rec (pid int, parch int)
    returns void
    language plpgsql
    strict
    as $$
    begin
      if exists (select * from found_deps where id = pid) then
        return;
      else
        if exists (select * from dependencies
        where (package = pid and dep in (select * from found_deps)
                or dep = pid and package in (select * from found_deps))
            and type = 'conflicts') then
          raise exception 'Depending on conflicting package!'
        end if
        insert into found_deps values (pid);
        perform find_dependencies_rec(latest_of_packages(
                array(select * from match_dependencies(dependencies.id, parch))),
            parch)
        from dependencies where dependencies.package = pid and type = 'depends';
      end if;
    end;
    $$;

create function find_dependencies (pid int)
    returns table (id int)
    language plpgsql
    strict
    as $$
    begin
      create temp table found_deps (id int not null) on commit drop;
      perform find_dependencies_rec(pid, (select arch from package_versions where package_versions.id = pid));
      delete from found_deps where found_deps.id = pid;
      return query select * from found_deps;
    end;
    $$;

-- access rights

grant connect on database dnoman to public;
grant select on all tables in schema public to public;
grant execute on function latest_of_packages (int[]) to public;
grant execute on function match_dependencies (int, int) to public;
grant execute on function find_dependencies (int) to public;

commit;
