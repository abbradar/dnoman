begin;

drop schema public cascade;
create schema public;

create table installed_packages (
        id serial primary key,
        name varchar not null,
        version varchar(32) not null,
        arch varchar(32) not null,
        dependency bool not null,
        unique (name, arch)
);

commit;
