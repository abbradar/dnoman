#!/bin/bash -e

repos=('testing' 'core' 'extra' 'community' 'multilib' 'multilib-testing')
archs=('i686' 'x86_64')

url="http://mirror.yandex.ru/archlinux"
srcdir="$(pwd)/repos"

cd "$srcdir"
rm -rf *

for repo in "${repos[@]}"; do
  for arch in "${archs[@]}"; do
    cd "$srcdir"
    mkdir -p "$repo/$arch"
    cd "$repo/$arch"
    curl -Of "$url/$repo/os/$arch/$repo.db" || continue
    curl -Of "$url/$repo/os/$arch/$repo.files" || true
    tar -xaf "$repo.db"    
    [ -f "$repo.files" ] && tar -xaf "$repo.files"
  done
done
