#!/usr/bin/ruby

require 'open-uri'
require 'set'
require 'date'
require 'sequel'
require 'logger'

$database = 'postgres://abbradar@localhost/dnoman'

Sequel.extension :core_extensions
$DB = Sequel.connect($database)

# We should not allow ":" really, it is workaround against malformed "perl-class-factory-util" package
$nchar = "[[:alnum:]@._+-:]"

$mirrorlist = 'https://www.archlinux.org/mirrorlist/?country=all&protocol=http&protocol=https&ip_version=4'
$incomplete = Set.new []

def parse_desc(s)
  Hash[s.scan(/^%(\w+)%\n((?:.+\n)+)/).map!{ |k, v| [k, v.lines.map!{ |s| s[0..-2] }] }]
end

def each_notnull(list)
  if not list.nil? then list.each { |e| yield(e) } end
end

def incomplete_package(name, repo)
  $DB[:packages].insert(name: name,
                        description: "",
                        url: "")
end

def package_byname(name, repo)
  otherpkg = $DB[:packages].first(name: name)
  otherpkg = otherpkg ? otherpkg[:id] : incomplete_package(name, repo)
end

def add_dependencies(pkg, list, type, repo)
  each_notnull list do |c|
    name, op, version = c.scan(/^(#{$nchar}+)([=<>]+)(.+)/)[0]
    if name.nil? then name = c end
    $DB[:dependencies].insert(package: pkg,
                              dep: package_byname(name, repo),
                              type: type,
                              version_constraint: op,
                              version: version)
  end
end

def process_package(path, fname, repo)
  desc, depends, files = ['desc', 'depends', 'files'].map!{ |n| parse_desc File.read(path + n) }
  pkg = $DB[:packages].first(name: desc['NAME'][0])
  pkg =
    if not pkg.nil? then
      pkg.update(description: desc['DESC'][0],
                 url: desc['URL'].nil? ? "" : desc['URL'][0])
      pkg[:id]
    else
      $DB[:packages].insert(name: desc['NAME'][0],
                            description: desc['DESC'][0],
                            url: desc['URL'][0])
    end
  each_notnull desc['GROUPS'] do |g|
    grp = $DB[:groups].first(name: g)
    grp = grp ? grp[:id] : $DB[:groups].insert(name: g, description: "")
    if $DB[:package_groups].first(group: grp, package: pkg).nil? then
      $DB[:package_groups].insert(group: grp,
                                  package: pkg)
    end
  end
  arch = $DB[:architectures].first(name: desc['ARCH'][0])
  if arch.nil? then
    arch = $DB[:architectures].insert(name: desc['ARCH'][0])
  else
    arch = arch[:id]
  end
  pkgver = $DB[:package_versions].first(package: pkg,
                                        repo: repo,
                                        arch: arch,
                                        version: desc['VERSION'][0],
                                        )
  if not pkgver.nil? then
    return
  else
    pkgver = $DB[:package_versions].insert(package: pkg,
                                           repo: repo,
                                           arch: arch,
                                           filename: fname,
                                           version: desc['VERSION'][0],
                                           license: desc['LICENSE'][0],
                                           packager: desc['PACKAGER'][0],
                                           ts: DateTime.strptime(desc['BUILDDATE'][0], '%s'),
                                           csize: Integer(desc['CSIZE'][0]),
                                           size: Integer(desc['ISIZE'][0]),
                                           sha256sum: desc['SHA256SUM'][0],
                                           )
  end
  add_dependencies(pkgver, depends['CONFLICTS'], 'conflicts', repo)
  add_dependencies(pkgver, depends['OFFERS'], 'offers', repo)
  add_dependencies(pkgver, depends['DEPENDS'], 'depends', repo)
  add_dependencies(pkgver, depends['PROVIDES'], 'provides', repo)
  each_notnull depends['REPLACES'] do |c|
    otherpkg.update(replaced_by: package_byname(c, repo))
  end
  
  each_notnull depends['OPTDEPENDS'] do |d|
    othername, depdesc = d.scan(/^(#{$nchar}+): (.+)/)[0]
    if othername.nil? then
      othername = d
      depdesc = ""
    end
    $DB[:dependencies].insert(package: pkgver,
                              dep: package_byname(othername, repo),
                              type: 'offers',
                              description: depdesc)
  end
  each_notnull files['FILES'] do |f|
    $DB[:files].insert(package: pkgver,
                       file_name: f)
  end
end

Dir['repos/*/'].each do |r|
  rname = r.scan(%r{^repos/([^/]+)/})[0]
  $DB.transaction do
    repo = $DB[:repositories].first(name: rname)
    if repo.nil? then
      repo = $DB[:repositories].insert(name: rname)
    else
      repo = repo[:id]
    end
    # Dir[r + '*/*/'].each do |f|
    Dir[r + 'x86_64/*/'].each do |f|
      puts "#{f}"
      fname = f.scan(%r{/([^/]+)/$})[0]
      process_package(f, fname, repo)
    end
  end
end

$DB.transaction do
  open($mirrorlist).each do |line|
    mirror = line.scan(/^#? *Server *= *(.*)$/)[0]
    if not mirror.nil? then
      mid = $DB[:mirrors].first(:url => mirror)
      if mid.nil? then
        mid = $DB[:mirrors].insert(:url => mirror)
        $DB[:repo_mirrors] << $DB[:repositories].select(mid.to_s.lit.as(:mirror),
                                                        :id.as(:repo),
                                                        '0'.lit.as(:priority))
      end
    end
  end
end
