#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require 'thread'
require 'gtk3'
require 'open-uri'
require 'set'
require 'date'
require 'sequel'
require 'logger'
require 'yaml'
require 'tmpdir'

Thread.abort_on_exception = true
Sequel.extension :core_extensions

#
# Globals!
#

$settings = YAML.load_file("settings.yml")

$DB = Sequel.connect($settings['database'])
$SRV = Sequel.connect($settings['server'])

$working = Mutex.new
$archname = `uname -m`.delete("\n")
$arch = $SRV[:architectures].first(name: $archname)[:id]

nchar = "[[:alnum:]@._+-:]"
ignoredfile = File.open("/etc/pacman.conf").read.scan(/^ *IgnorePkg *=(.*)/)
$ignored = if ignoredfile.nil? then
             []
           else
             ignoredfile[0][0].scan(/#{nchar}+/)
           end
$ignored << "mesa" << "libdrm" << "freetype2" << "llvm-libs"
$ignored = $SRV[:packages].select(:id).where(name: $ignored).all.map! { |n| n[:id] }

#
# Misc
#

class WorkError < StandardError  
end

def do_work(s)
  to_disable = [ 'pkgInstall',
                 'pkgRemove',
                 'pkgRemove',
                 'filterAdd',
                 'filterRemove',
                 'filterClear',
                 'localUpdate',
               ]
  Thread.new do
    $working.synchronize do
      to_disable.each { |n| $builder[n].sensitive = false }
      $builder['statusbar'].push $status, s
      begin
        yield
      rescue WorkError => e
        $builder['statusbar'].push $status, e.message
        sleep 3
        $builder['statusbar'].pop $status
      ensure
        $builder['statusbar'].pop $status
        to_disable.each { |n| $builder[n].sensitive = true }
      end
    end 
  end
end

#
# Processing locals
#

def parse_desc(s)
  Hash[s.scan(/^%(\w+)%\n((?:.+\n)+)\n/).map!{ |k, v| [k, v.lines.map!{ |s| s[0..-2] }] }]
end

def each_notnull(list)
  if not list.nil? then list.each { |e| yield(e) } end
end

def process_local(path)
  desc = parse_desc (File.read (path + 'desc'))
  pkg = $SRV[:package_versions].left_join(:packages, id: :package).
    left_join(:architectures, id: :package_versions__arch).
    select(:packages__name.as(:pkgname),
           :version,
           :architectures__name.as(:archname)).
    first(packages__name: desc['NAME'][0],
          architectures__name: desc['ARCH'][0],
          version: desc['VERSION'][0])
  if not pkg.nil? then
    $DB[:installed_packages].insert(name: pkg[:pkgname],
                                    version: pkg[:version],
                                    arch: pkg[:archname],
                                    dependency: desc.key?('REASON'))
  end
end

def load_locals
  do_work "Загрузка локальных пакетов" do
    $locals = Hash.new
    $DB[:installed_packages].each do |i|
      id = $SRV[:package_versions].
        left_join(:packages, id: :package).
        left_join(:architectures, id: :package_versions__arch).
        select(:packages__id.as(:pkgid), :package_versions__id.as(:pkgverid), :version).
        first(packages__name: i[:name], version: i[:version], architectures__name: i[:arch])
      if not id.nil? then
        $locals[id[:pkgid]] = { version: id[:version], id: id[:pkgverid] }
      end
    end

    get_package_list
  end
end


#
# Update package list
#

def get_package_list
  do_work "Получение пакетов с сервера" do
    data = $builder['packages']
    data.clear
    set = $SRV[:packages].
      left_join(:package_archs, package: :packages__id, arch: $arch).
      left_join(:package_versions, id: :last_version).
      select(:packages__id, :name, :version).
      where($SRV[:package_versions].left_join(:repositories, id: :repo).
            where(package: :packages__id, arch: $arch, active: true).exists).
      where(replaced_by: nil).
      order_by(:name)
    $filters.each { |m, p, i| set = i[0][set] }
    set.each do |m|
      local = $locals[m[:id]]
      c = data.append nil
      c.set_value 0, m[:id].to_i
      c.set_value 1, local.nil? ? ' ' : 'i'
      c.set_value 2, m[:name]
      c.set_value 3, local.nil? ? '' : local[:version]
      c.set_value 4, m[:version]
      data.append c
    end
  end
end

#
# Main menu
#

def app_exit
  $working.synchronize do
    File.open('settings.yml', 'w') { |f| f.write $settings.to_yaml }
    Gtk.main_quit
  end
end

def local_update
  do_work "Синхронизация локальных пакетов" do

    $DB[:installed_packages].delete
    
    Dir['/var/lib/pacman/local/*/'].each do |r|
      $DB.transaction do
        process_local r
      end
    end
    load_locals
  end
end

#
# Packages
#

def pkgs_expanded(w, i, p)
  if $working.locked? then
    $builder['packagesList'].collapse_row p
  else
    $working.synchronize do
      if i.first_child[3].nil? then
        data = $builder['packages']
        data.remove(i.first_child)
        $SRV[:package_versions].select(:package_versions__id, :version, :repositories__name).
            left_join(:repositories, id: :package_versions__repo).
            where(package: i[0], active: true).each do |m|
          c = data.append i
          c.set_value 0, m[:id].to_i
          c.set_value 1, ($locals.has_key? i[0] and $locals[i[0]][:id] == m[:id]) ? 'i' : ' '
          c.set_value 3, m[:version]
          c.set_value 5, m[:name]
        end
      end
    end
  end
end

def pkg_selected
  p = $builder['packagesList'].selection.selected
  if not p.nil? and not $working.locked? then
    $working.synchronize do
      if p.has_child? then
        $builder['details'].buffer.text = $SRV[:packages].first(id: p[0])[:description]
      else
        data = $SRV[:package_versions].first(id: p[0])
        description = $SRV[:packages].first(id: p.parent[0])[:description]
        depends = $SRV[:dependencies].where(package: p[0]).left_join(:packages, id: :dep)
        str = "#{description}\n\n" +
          "Лицензия: #{data[:license]}\n" +
          "Создатель пакета: #{data[:packager]}\n" +
          "Размер архива: #{data[:csize]}\n" +
          "Распакованный размер: #{data[:size]}\n" +
          "Время: #{data[:ts]}\n"

        dep_print = ->(n, d) do
          s = d.all.map! do |dep|
            r = dep[:name]
            if not dep[:version_constraint].nil? then
              r += " " + dep[:version_constraint] + " " + dep[:version]
            end
            r
          end
          s = s.join ", "
          if not s.empty? then
            str += "#{n}: #{s}\n"
          end
        end

        dep_print["Предлагает", depends.where(type: 'offers')]
        dep_print["Зависит", depends.where(type: 'depends')]
        dep_print["Конфликтует", depends.where(type: 'conflicts')]
        dep_print["Предоставляет", depends.where(type: 'provides')]
        
        files = $SRV[:files].grep(:file_name, '%/').invert.
          where(package: p[0]).select(:file_name).all.
          map! { |n| n[:file_name] }.join "\n"
        str += "\nФайлы:\n#{files}"

        $builder['details'].buffer.text = str
      end
    end
  end
end

def install_package
  p = $builder['packagesList'].selection.selected
  if not p.nil? then
    pkgid = if p.has_child? then
              $SRV[:package_archs].first(package: p[0], arch: $arch)[:last_version]
            else
              p[0]
            end

    def get_pkginfo(d)
      d.left_join(:packages, id: :package).
        left_join(:architectures, id: :package_versions__arch).
        left_join(:repositories, id: :package_versions__repo).
        left_join($SRV[:repo_mirrors].
                    group_by(:mirror, :repo).
                    select(:mirror, :repo, :max.sql_function(:priority)).
                    distinct(:repo),
                  repo: :package_versions__repo).
        left_join(:mirrors, id: :mirror).
        select(:packages__id___pkgid, :package_versions__id___pkgverid, :packages__name,
               "\"filename\" || ? || \"architectures\".\"name\" || ?".lit('-', '.pkg.tar.xz').as(:fname),
               :architectures__name___arch, :repositories__name___repo, :mirrors__url, :version)
    end
      
    pkg = get_pkginfo($SRV[:package_versions].where(package_versions__id: pkgid)).first
    if not $locals.has_key? pkg[:pkgid] or $locals[pkg[:pkgid]][:id] != pkg[:pkgverid] then
      do_work "Установка пакета" do
        pkgvers = $locals.map { |_, w| w[:id] } << pkg[:pkgverid]
        deps = get_pkginfo($SRV.select(:find_dependencies.sql_function(pkg[:pkgverid]).as(:id)).
                           from_self(alias: :needed).
                           left_join(:package_versions, id: :id)).
          # exclude(packages__id: $ignored & $locals.keys).
          exclude(package_versions__id: pkgvers).
          exclude(packages__id: $ignored).
          exclude($SRV[:dependencies].where(dep: :packages__id,
                                            package: pkgvers, type: 'provides').exists)
        deps = deps.all
        all = deps.clone << pkg
        Dir.mktmpdir do |tmp|
          $builder['statusbar'].push $status, "Загрузка пакетов"
          begin
            all.each do |p|
              $builder['statusbar'].push $status, "Загружаю #{p[:fname]}"
              url = "#{p[:url].sub!("$repo", p[:repo]).sub!("$arch", p[:arch])}/#{p[:fname]}"
              begin
                open(url, "rb") do |u|
                  open("#{tmp}/#{p[:fname]}", "wb") do |f|
                    f.write(u.read)
                  end
                end
              rescue OpenURI::HTTPError => e
                raise WorkError.new, "Ошибка загрузки #{p[:fname]}: #{e.message}"
              ensure
                $builder['statusbar'].pop $status
              end
            end
          ensure
            $builder['statusbar'].pop $status
          end
          $builder['statusbar'].push $status, "Установка в систему"
          begin
            pkgs = all.map { |x| "#{tmp}/#{x[:fname]}" }.join(' ')
            puts "pacman -U #{pkgs}"
            if not system("kdesu pacman -Udd --noconfirm #{pkgs}") then
              raise WorkError.new, "Ошибка установки"
            end
            depslist = deps.map { |x| x[:name] }.join(' ')
            if not depslist.empty? then
              `kdesu pacman -D --asdep #{depslist}`
            end
          ensure
            $builder['statusbar'].pop $status
          end
        end
        
        def installed_insert (p, dep)
          puts p
          $DB[:installed_packages].insert(name: p[:name],
                                          version: p[:version],
                                          arch: p[:arch],
                                          dependency: dep)
        end
        
        $DB.transaction do
          installed_insert pkg, false
          deps.each { |p| installed_insert p, true }
        end
        all.each do |p|
          $locals[p[:pkgid]] = { id: p[:pkgverid], version: p[:version] }
        end
        
        get_package_list
      end
    end
  end
end

def remove_package
  p = $builder['packagesList'].selection.selected
  puts p
  if not p.nil? and p[1] == 'i' then
    do_work "Удаление пакетов" do
      pkg = if p.has_child? then
              $locals[p[0]][:id]
            else
              p[0]
            end
      
      def get_info(p)
        p.left_join(:packages, id: :package).
          left_join(:architectures, id: :package_versions__arch).
          exclude($SRV[:dependencies].where(dep: :packages__id, type: "depends",
                                            package: $locals.map { |_, w| w[:id] }).exists).
          select(:packages__name___name, :packages__id___pkgid, :package_versions__id___pkgverid,
                 :architectures__name___arch)
      end

      pkg = get_info($SRV[:package_versions].where(package_versions__id: pkg)).first
      
      deps = get_info($SRV.select(:find_dependencies.sql_function(pkg[:pkgverid]).as(:id)).
                      from_self(alias: :unneeded).
                      left_join(:package_versions, id: :id)).all
      unneeded = Set.new $DB[:installed_packages].where(name: deps.map { |n| n[:name] },
                                                        arch: $archname, dependency: true).
        select(:name).all.map! { |n| n[:name] }
      deps.select! { |n| unneeded.member? n[:name] }

      pkgs = deps << pkg
      list = pkgs.map { |n| n[:name] }.join ' '
      puts "pacman -Rdd --noconfirm #{list}"
      if not system("kdesu pacman -Rdd --noconfirm #{list}") then
        raise WorkError.new, "Ошибка удаления"
      end
      $DB.transaction do
        pkgs.each do |p|
          puts p
          $DB[:installed_packages].where(name: p[:name], arch: p[:arch]).delete
        end
      end
      pkgsh = Set.new pkgs.map { |n| n[:pkgid] }
      $locals.delete_if { |k, w| pkgsh.member? k }
      
      get_package_list
    end
  end
end

#
# Filters
#

def clear_filters
  $builder['filters'].clear
  get_package_list
end

def add_filter
  if $builder['filterName'].active? and not $builder['filterNameLike'].text.empty? then
    c = $filters.append
    cp = $builder['filterNameLike'].text
    c.set_value 0, ->(x) { x.where("position(? in name) != 0", cp ) }
    c.set_value 1, "'#{$builder['filterNameLike'].text}' в названии"
    $builder['filterNameLike'].text = ""

    get_package_list
  elsif $builder['filterGroup'].active? and $builder['groupsList'].active != -1 then
    group = $builder['groupsList'].active_iter
    c = $filters.append
    c.set_value 0, ->(x) { x.where($SRV[:package_groups].
                                   where(package: :packages__id,
                                         group: group[0]).
                                   exists) }
    c.set_value 1, "Входит в группу '#{group[1]}'"

    get_package_list
  end
end

def remove_filter
  $builder['filtersList'].selection.selected_each { |m, p, i| m.remove(i) }
  get_package_list
end

#
# Main
#

Gtk.init

$builder = Gtk::Builder.new
$builder.add_from_file("gui.glade")
$builder.connect_signals { |n| method(n) }

# Create additional objects
$filters = Gtk::ListStore.new Proc, String

# Status bar
arch = $builder['statusbar'].get_context_id "arch"
$builder['statusbar'].push arch, $archname
$status = $builder['statusbar'].get_context_id "statuses"
$builder['statusbar'].push $status, "Готов"

# Get locals and packages (another thread)
load_locals

# Packages
[ Gtk::TreeViewColumn.new("С", Gtk::CellRendererText.new, text: 1),
  Gtk::TreeViewColumn.new("Название", Gtk::CellRendererText.new, text: 2),
  Gtk::TreeViewColumn.new("Версия", Gtk::CellRendererText.new, text: 3),
  Gtk::TreeViewColumn.new("Последняя версия", Gtk::CellRendererText.new, text: 4),
  Gtk::TreeViewColumn.new("Репозиторий", Gtk::CellRendererText.new, text: 5),
].each { |c| $builder['packagesList'].append_column(c) }

# Filters
$builder['filtersList'].model = $filters
$builder['filtersList'].append_column(Gtk::TreeViewColumn.new("Фильтр", Gtk::CellRendererText.new, text: 1))

# Groups
groups_r = Gtk::CellRendererText.new
$builder['groupsList'].pack_start(groups_r, true)
$builder['groupsList'].add_attribute(groups_r, "text", 1)
groups = $builder['groups']
$SRV[:groups].select(:id, :name).each do |g|
  c = groups.append
  c.set_value 0, g[:id].to_i
  c.set_value 1, g[:name]
end

# Run!
$builder['mainWin'].show_all
Gtk.main
